var app = angular.module('myApp', []);


app.controller('myCtrl', function($scope) {

    $scope.toDisp = 'idx';
    $scope.logVar = false;
    $scope.idxClk = "btn btn-outline-primary";
    $scope.jeuClk = "btn btn-primary";
    $scope.mnuClk= "btn btn-primary";
    $scope.frmClk = "btn btn-primary";

    $scope.test = function () {
        alert('bonjour');
    }

    $scope.log = function () {
        $scope.logVar = !$scope.logVar;
    }

    $scope.onClick = function (btnVar) {
        $scope.toDisp = btnVar;
        switch (btnVar) {
            case 'idx' :
                $scope.idxClk = "btn btn-outline-primary";
                $scope.jeuClk = "btn btn-primary";
                $scope.mnuClk= "btn btn-primary";
                $scope.frmClk = "btn btn-primary";
                break;
            case 'jeu' :
                $scope.idxClk = "btn btn-primary";
                $scope.jeuClk = "btn btn-outline-primary";
                $scope.mnuClk= "btn btn-primary";
                $scope.frmClk = "btn btn-primary";
                break;
            case 'mnu' :
                $scope.idxClk = "btn btn-primary";
                $scope.jeuClk = "btn btn-primary";
                $scope.mnuClk= "btn btn-outline-primary";
                $scope.frmClk = "btn btn-primary";
                break;
            case 'frm' :
                $scope.idxClk = "btn btn-primary";
                $scope.jeuClk = "btn btn-primary";
                $scope.mnuClk= "btn btn-primary";
                $scope.frmClk = "btn btn-outline-primary";
                break;
        }
    }
});